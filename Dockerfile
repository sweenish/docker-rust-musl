FROM rust:latest

RUN apt-get update \
  && apt-get install -y --no-install-recommends \
     musl musl-dev musl-tools \
  && rm -rf /var/lib/apt/lists/*

RUN rustup target add x86_64-unknown-linux-musl \
  && rustup component add rust-analyzer \
  && mkdir -p $HOME/.cargo/bin \
  && ln -s $(rustup which rust-analyzer) $HOME/.cargo/bin/rust-analyzer \
  && printf "export COLORTERM=truecolor\nexport PATH=$HOME/.cargo/bin:$PATH" >> ~/.bashrc \
  && cargo install taplo-cli --features lsp  --locked \
  && REPO="https://github.com/helix-editor/helix.git" \
  && git clone $REPO \
  && cd helix \
  && git checkout -b $(git ls-remote --tags --refs --sort=taggerdate $REPO | tail -n1 | cut -d/ -f3) \
  && cargo install --path helix-term \
  && mkdir -p $HOME/.local/helix \
  && mv runtime $HOME/.local/helix/ \
  && mkdir -p $HOME/.config/helix \
  && ln -s $HOME/.local/helix/runtime $HOME/.config/helix/runtime \
  && printf \
    "[editor]\nbufferline = \"multiple\"\nline-number = \"relative\"\ncolor-modes = true\n" \
    > $HOME/.config/helix/config.toml
