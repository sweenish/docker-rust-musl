# rust-musl
This container adds the musl developer tools and rustup target, rust-analyzer,
taplo, and helix-editor to the official rust container. Building rust projects
against the musl target will allow you to have statically linked executables.

## Installation
```
git clone https://gitlab.com/sweenish/docker-rust-musl
cd docker-rust-musl
docker build -t rust-musl .
```

## Use
`docker run -it -v <path/to/your/project>:<project> rust-musl`

## Building in the container
`cargo build --target=x86_64-unknown-linux-musl`
